close all
clear all
clc
%% entradas nuevas 
load('datos.mat')
[x_order,k] = sort(x(1,:));
y_order = y(k);
%datos de entrenamiento
x_ent = x(1:35);
y_ent = y(1:35);
% ordenar datos de entrenamiento 
[x_ent_order,kent] = sort(x_ent(1,:));
y_ent_order = y_ent(kent);
% datos de validacion
x_val = x(36:50);
y_val = y(36:50);
% ordenar datos de validacion 
[x_val_order,kval] = sort(x_val(1,:));
y_val_order = y_val(kval);
% definir funciones de entrenamiento
funciones = ["trainlm" "trainbr" "trainbfg" "trainrp" "trainscg" "traincgb" "traincgf" "traincgp" "trainoss" "traingdx" "traingdm" "traingd"];
% definir iteciones de funciones
for j =1:1:length(funciones)
% definir numero de neuronas
for i=1:1:100
% crear red
net = fitnet(i,funciones(j));
% entrenar la red
[net, tr] = train(net,x_ent,y_ent);
% validar modelo
y_ent_prueba = net(x_ent);
% valida si es el primer ciclo y guarda el primer performance
if i == 1 && j == 1
    % guardar performance
perf = perform(net,y_ent_prueba,y_ent);
% guardar la red
save('redmejor.mat','net');    
end 
% valida si ya paso la primera iteracion
if i > 1
% guarda el performance en una variable temporal
perf1 = perform(net,y_ent_prueba,y_ent);
% compara si el anterior performance es mejor que el actual
if perf1 < perf
    % si se cumple la condicion  guarda el performance temporal como el
    % nuevo mejor performance
 perf = perf1;
 % guarda la red del mejor performance
 save('redmejor.mat','net');  
 fun = funciones(j);
 neuronas = i;
end
end
end 

%% graficar mejor red
load('redmejor.mat')
y_val_prueba = net(x_val);
figure;
subplot(2,2,1)
stem(x_val,y_val_prueba)
hold on
stem(x_val,y_val)
xlabel('datos')
ylabel('valor')
legend('dato esperado','dato validacion');
title('comparacion datos validacion y validados')
%view(net)
%% grafica datos validacion 
y_val_order_prueba = net(x_val_order);
subplot(2,2,2)
plot(x_val_order,y_val_order_prueba)
hold on 
plot(x_val_order,y_val_order)
xlabel('datos')
ylabel('valor')
legend('datos calculados','datos de validacion');
title('grafica datos validacion ordenados')

%% grafica datos de entrenamiento
y_ent_order_prueba = net(x_ent_order);
subplot(2,2,3)
plot(x_ent_order,y_ent_order_prueba)
hold on 
plot(x_ent_order,y_ent_order)
xlabel('datos')
ylabel('valor')
legend('datos calculados','datos de entrenamiento');
title('grafica datos entrenamiento ordenados')
%% grafica set de datos completo
y_order_prueba = net(x_order);
subplot(2,2,4)
plot(x_order,y_order_prueba)
hold on 
plot(x_order,y_order)
xlabel('datos')
ylabel('valor')
legend('datos calculados','datos completos');
title('grafica set de datos ordenado')
saveas(gcf,['mejor_', num2str(neuronas),'_neuronas_funcion_',num2str(j),'.png'])
end